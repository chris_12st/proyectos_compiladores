#include<stdlib.h>
#include<stdio.h>
#include<string.h>

typedef struct node_s {
	void *data;
	struct node_s *next;	
} NODE;

/*-----------------DEFINIÇÃO DO NÓ DA TABELA DE SÍMBOLOS---------------------------------*/
typedef struct node_symbol_table {
		char *nome_identificador;
		int tipo_identificador; /*1-variavel
					 2-função*/
		int tipo_variavel; /*1-char
				    2-double
				    3-float
				    4-int
   				    0-Se tipo_identificador == 2(Se for função o identificador)*/
		
		int tipo_retorno; /*1-char
				    2-double
				    3-float
				    4-int
				    5-void
				    0-Se tipo_identificador == 1(Se for variavel o identificador)*/ 
		struct node_symbol_table *next;	
	}NODE_SYMBOL_TABLE;			
/*--------------------------------------------------------------------------------------*/

/*Cria um nó contendo a STRUCT, e retorna esse nó criado, com NEXT = NULL
  retorna o nó criado*/
NODE *list_create(void *data)
{
	NODE *node;
	if(!(node=malloc(sizeof(NODE)))) return NULL;
	node->data=data;
	node->next=NULL;
	return node;
}
/*Insere um nó, que é criado com a informação void* data, após o nó informado como parametro
  retorna o nó inserido*/
NODE *list_insert_after(NODE *node, void *data)
{
	NODE *newnode;
        newnode=list_create(data);
        newnode->next = node->next;
        node->next = newnode;
	return newnode;
}
/*Insere um nó, que é criado com a informação void* data, antes do primeiro elemento da lista
  retorna o nó inserido*/
NODE *list_insert_beginning(NODE *list, void *data)
{
	NODE *newnode;
        newnode=list_create(data);
        newnode->next = list;
	return newnode;
}
/*Remove o nó passado como parametro, na lista passada como parametro.
  Retorna 0 se o nó foi removido.
  Retorna -1 se o nó não foi encontrado ou foi o ultimo nó da lista.
*/
int list_remove(NODE *list, NODE *node)
{
	while(list->next && list->next!=node) list=list->next;
	if(list->next) {
		list->next=node->next;
		free(node);
		return 0;		
	} else return -1;
}
/* A função passada como parametro é uma função de impressão de STRING.
   Faz o print de todos os nodes->data usando a função passada como parametro, de todos
   os elementos da lista.
   Utiliza a função PRINT_NODE().
*/
int list_foreach(NODE *node, int(*func)(void*))
{
	while(node) {
		if(func(node->data)!=0) return -1;
		node=node->next;
	}
	return 0;
}
/* Procura um nó com a informação passada em void* data na lista informada no parametro NODE* node.
   Se achou um nó contendo a informação passada, retorna esse nó.
   Se não achou um nó contendo a informação passada, retorna NULL.
   Utiliza a função FINDSTRING().
   */
NODE *list_find(NODE *node, int(*func)(void*,void*), void *data)
{
	while(node) {
		if(func(node->data, data)>0) return node;
		node=node->next;
	}
	return NULL;
}

/*Retorna o ultimo nó da lista.*/
NODE *retornar_ultimo_elemento(NODE *lista) {
	while(lista) {
		if(lista->next == NULL)
			return lista;
		lista = lista->next;
	}
}

/*Mostrar dados de um nó da tabela de simbolos*/
int print_node(void *data) {
	NODE_SYMBOL_TABLE *node = (NODE_SYMBOL_TABLE*)data;

	printf("%s\n", node->nome_identificador);
	printf("tipo_identificador = %d\n", node->tipo_identificador);
	printf("tipo_variavel = %d\n", node->tipo_variavel);
	printf("tipo_retorno = %d\n", node->tipo_retorno);
	printf("\n"); 
	return 0;	
}

/* Função para comparar duas STRINGS.
   Retorna 1 se as strings forem iguais.
   Retorna 0 se as strings forem diferentes.
*/
int findstring(void *listdata, void *searchdata)
{
	NODE_SYMBOL_TABLE *tabela_simbolos = (NODE_SYMBOL_TABLE*)listdata;

	return strcmp(tabela_simbolos->nome_identificador, (char*)searchdata)?0:1;
}

/*Insere um elemento na lista simplesmente encadeada.
  Retorna 1 se o elemento foi inserido com sucesso.
  Retorna -1 se houver um erro na inserção do elemento.*/
int inserir_elemento(NODE *list, void *data) {

	NODE *novo_no = list_create(data);

	if(list == NULL)
		return -1;
	
	while(list) {
		if(list->next == NULL) {
			list->next = novo_no;
			break;
		}		
		list = list->next;
	}

	return 1;
}

/*Cria uma STRUCT com os dados de um IDENTIFICADOR*/
NODE_SYMBOL_TABLE *criar_registro(char *nome_identificador, int tipo_identificador, int tipo_variavel, int tipo_retorno) {
	
	NODE_SYMBOL_TABLE *registro = (NODE_SYMBOL_TABLE*)malloc(sizeof(NODE_SYMBOL_TABLE));
	
	registro->nome_identificador = nome_identificador;
	registro->tipo_identificador = tipo_identificador;
	registro->tipo_variavel = tipo_variavel;
	registro->tipo_retorno = tipo_retorno;	

	return registro;
}
