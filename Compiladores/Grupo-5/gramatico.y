/*Analizador Sintactico*/
%{
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define YYSTYPE char*
#include "tableSymbole.h"
#include "instructions.h"
#include "jump_stack.h"
#include "function_stack.h"


int stack_pointer_adress = 1000;
int yyerror(char *s);
int line_number = 1;
int if_to_fill = 0;
extern FILE *yyin;
extern char *yytext;
%}
%token	CHAR INT FLOAT DOUBLE VOID

%token	IF WHILE ELSE DO FOR BREAK RETURN

%token  ENTERO  CARACTER REAL IDENTIFICADOR

%token	PARENTESIS_IZQUIERDO PARENTESIS_DERECHO LLAVE_IZQUIERDA LLAVE_DERECHA

%token	MAS MENOS MULT DIVISION INCREMENTO DECREMENTO	

%token	MAYOR MENOR MAYOR_IGUAL MENOR_IGUAL IGUAL

%token	ATRIBUCION PUNTO_COMA COMA

%token 	AND OR NOT EOL

//ype 	<tipo>tipo_dato
%%
input:
|
input linea 
;

linea:
exprecion EOL      //{printf("Linea Valida\n"); line_number++;}
;

exprecion:
declaracion_variable
|
declaracion_variable_inicializado_entero
|
declaracion_variable_inicializado_real
|
declaracion_variable_inicializado_caracter
|
bucleW
|
bucleF
|
abrir
|
cerrar
|
asignacion
|
desicion
|
funcionMain 
|
error    { yyerrok;  }
;
declaracion_variable:
tipo_dato IDENTIFICADOR PUNTO_COMA 
|
tipo_dato variables PUNTO_COMA
|
{}
;
variables:
IDENTIFICADOR COMA IDENTIFICADOR 
|
variables COMA IDENTIFICADOR

;
declaracion_variable_inicializado_entero:
tipo_dato IDENTIFICADOR ATRIBUCION ENTERO PUNTO_COMA /*{ 
				if (ts_push($2,$1)==-1){
					yyerror("Variable already exist at ligne ");return 1;
				}}*/
|
tipo_dato variables_inicializadas_entero PUNTO_COMA  
				
;
variables_inicializadas_entero:
IDENTIFICADOR ATRIBUCION ENTERO 
|
variables_inicializadas_entero COMA IDENTIFICADOR ATRIBUCION ENTERO 
;
declaracion_variable_inicializado_real:
tipo_dato IDENTIFICADOR ATRIBUCION REAL PUNTO_COMA 
|
tipo_dato variable_inicializada_real PUNTO_COMA 
;
variable_inicializada_real:
IDENTIFICADOR ATRIBUCION REAL 
|
variable_inicializada_real COMA IDENTIFICADOR ATRIBUCION REAL 
;
declaracion_variable_inicializado_caracter:
tipo_dato IDENTIFICADOR ATRIBUCION CARACTER PUNTO_COMA 
|
tipo_dato variables_inicializadas_caracter PUNTO_COMA
;
variables_inicializadas_caracter:
IDENTIFICADOR ATRIBUCION CARACTER 
|
variables_inicializadas_caracter COMA IDENTIFICADOR ATRIBUCION CARACTER 
;
tipo_dato:
INT 
|
CHAR
|
DOUBLE 
|
FLOAT 
;
bucleW:
WHILE PARENTESIS_IZQUIERDO expresionl PARENTESIS_DERECHO
|
WHILE PARENTESIS_IZQUIERDO expresionl PARENTESIS_DERECHO PUNTO_COMA
;
expresionl:
IDENTIFICADOR MAYOR IDENTIFICADOR {
//	stack_push_sup($1,$3,1);
}
|
IDENTIFICADOR MAYOR ENTERO  {
//	stack_push_sup($1,$3,1);
}
|
IDENTIFICADOR MENOR IDENTIFICADOR {
//	stack_push_inf($1,$3,1);
}
|
IDENTIFICADOR MENOR ENTERO  {
//	stack_push_inf($1,$3,1);
}
|
IDENTIFICADOR MENOR_IGUAL  IDENTIFICADOR 
|
IDENTIFICADOR MENOR_IGUAL  ENTERO
|
IDENTIFICADOR MAYOR_IGUAL  IDENTIFICADOR
|
IDENTIFICADOR MAYOR_IGUAL  ENTERO
|
IDENTIFICADOR IGUAL IDENTIFICADOR  {
//	int tmp = ts_add_temp();
//	$$=tmp;
//	stack_push_equ_t(tmp,$1,$3);
//	stack_push_add_cr(tmp);

}
|
IDENTIFICADOR IGUAL ENTERO {
//	int tmp = ts_add_temp();
//	$$ = tmp;
//	stack_push_equ_t(tmp,$1,$3);
//	stack_push_add_cr(tmp);

}
;
bucleF:
FOR PARENTESIS_IZQUIERDO exprecionF PARENTESIS_DERECHO  
;
exprecionF:
IDENTIFICADOR ATRIBUCION ENTERO PUNTO_COMA{
//	stack_push_afc_cr(0);
//	while_add_from_to(get_number_of_line());
} expresionl {
//        if_add_from_where(get_number_of_line());
//        stack_push_push_cr();
} PUNTO_COMA  IDENTIFICADOR INCREMENTO /*{
        if(get_addr_from($9) != -1){
  //                   stack_push_cop(get_addr_from($9),$1);
 //                 ts_pop_addr($1);
         }else{
                     yyerror("Bad affectation at ligne ");
                     return 1;
         }

//	stack_push_pop_cr();
//        stack_push_nop();
        
//	while_fill_from_where(get_number_of_line());
       if_fill_from_to(get_number_of_line()+1);}*/

|
declaracion_variable_inicializado_entero PUNTO_COMA expresionl PUNTO_COMA IDENTIFICADOR INCREMENTO
;
abrir:
LLAVE_IZQUIERDA
;
cerrar:
LLAVE_DERECHA
;
operacionA:
IDENTIFICADOR MAS IDENTIFICADOR {/* stack_push_add($1,$1,$3); ts_pop_addr($3);*/}
|
IDENTIFICADOR MAS ENTERO {/* stack_push_add($1,$1,$3); ts_pop_addr($3);*/}
|
IDENTIFICADOR MAS REAL { /*stack_push_add($1,$1,$3); ts_pop_addr($3);*/}
|
IDENTIFICADOR MENOS IDENTIFICADOR {/*stack_push_sub($1,$1,$3);ts_pop_addr($3);*/} 
|
IDENTIFICADOR MENOS ENTERO {/*stack_push_sub($1,$1,$3);ts_pop_addr($3);*/} 
|
IDENTIFICADOR MENOS REAL {/*stack_push_sub($1,$1,$3);ts_pop_addr($3);*/} 
|
IDENTIFICADOR MULT IDENTIFICADOR {/*stack_push_mul($1,$1,$3);ts_pop_addr($3);*/}
|
IDENTIFICADOR MULT ENTERO {/*stack_push_mul($1,$1,$3);ts_pop_addr($3);*/}
|
IDENTIFICADOR MULT REAL {/*stack_push_mul($1,$1,$3);ts_pop_addr($3);*/}
|
IDENTIFICADOR DIVISION IDENTIFICADOR {/*stack_push_div($1,$1,$3);ts_pop_addr($3);*/}
|
IDENTIFICADOR DIVISION ENTERO {/*stack_push_div($1,$1,$3);ts_pop_addr($3);*/}
|
IDENTIFICADOR DIVISION REAL {/*stack_push_div($1,$1,$3);ts_pop_addr($3);*/}
|
ENTERO MENOS ENTERO {printf("li $a0 %s \nsw $a0 0($sp) \n addiu $sp $sp -4 \nli $a0 %s \nlw $t1 4($sp) \n sub $a0 $t1 $a0\naddiu $sp $sp 4\n",$1,$3);}
|
ENTERO MAS ENTERO {printf("li $a0 %s \nsw $a0 0($sp) \n addiu $sp $sp -4 \nli $a0 %s \nlw $t1 4($sp) \n addiu $a0 $t1 $a0\naddiu $sp $sp 4\n",$1,$3);}

;
asignacion:
IDENTIFICADOR ATRIBUCION operacionA PUNTO_COMA
;
desicion:
IF PARENTESIS_IZQUIERDO expresionl PARENTESIS_DERECHO
|
ELSE 
|
ELSE LLAVE_IZQUIERDA exprecion LLAVE_DERECHA 
|
ELSE LLAVE_IZQUIERDA
|
IF PARENTESIS_IZQUIERDO expresionl PARENTESIS_DERECHO LLAVE_IZQUIERDA 
|
IF PARENTESIS_IZQUIERDO expresionl PARENTESIS_DERECHO LLAVE_IZQUIERDA exprecion LLAVE_DERECHA
;
funcionMain:
tipo_dato IDENTIFICADOR PARENTESIS_IZQUIERDO  PARENTESIS_DERECHO{
                                //pop_symb_zone();
				//print_all_assembler_instructions();
				//parse_and_modify_file("toto","asm_with_jump");
} ;
; 
%%
int main(int argc,char **argv)
{
	
	if (argc>1)
	{
		yyin=fopen(argv[1],"rt");
	}
	else
	{
		//yyin=stdin;
		yyin=fopen("ejemplo.txt","rt");
	}		

	yyparse();
	return 0;
}
yyerror (char *s)
{
  extern int yylineno;
  printf ("%s\n", s);
  printf ("Linea actual: %d\n",yylineno);
}
int yywrap()
{
 return 1;
}
